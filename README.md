# wirvsvirus-frontend

[![Build Status](https://gitlab.com/gersta/wirvsvirus-frontend/badges/master/pipeline.svg)](https://gitlab.com/gersta/wirvsvirus-frontend)

Diese Repository hostet das Angular frontend für DigiTafel im Zuge des [#WirvsVirusHack Hackathons](https://wirvsvirushackathon.org/) der Bundesregierung.

Hier geht's zur [Live-Version](http://dmxlolj5irb17.cloudfront.net/auth/register) der Anwendung.

Der Backend Code kann in der dedizierten [Backend Code Repository](https://gitlab.com/gersta/wirvsvirus-backend) gefunden werden. Zusätzlich
steht die [Swagger Spezifikation](http://ec2-3-121-188-164.eu-central-1.compute.amazonaws.com/swagger-ui.html#/users-api-controller) der Restful API zur Verfügung.

Unser Team findet ihr auf [DevPost](https://devpost.com/software/013_tafel_reorganisation_digitafel).

Und hier ist unser [Youtube Kanal](https://www.youtube.com/channel/UCPzmvuwcBwtZHXPGktBN-FA/featured?view_as=subscriber).

## Ziele
Es gibt ganz unterschiedliche Gründe, warum im Zusammenhang mit dem Coronavirus aktuell viele Tafeln schließen müssen. Einer der Gründe dafür ist, dass dort viele Menschen zu den Abholzeiten aufeinandertreffen und dass die ein Großteil der ehrenamtlichen Helfer meist zur Risikogruppe gehört. Außerdem können einige Menschen aufgrund von Quarantäne nicht ihr Haus verlassen, um zur Tafel zu gehen.

Dies ist ein großes Problem für viele Menschen, die auf die Unterstützung der Tafel angewiesen sind. Unsere Inspiration war es, eine Plattform zu schaffen, die auf digitalem Wege eine Koordination zwischen Kunden, der Tafelleitung und in Zukunft auch ehrenamtlichen Helfern ermöglicht, um gezielt Abholung, Lieferung und Vorbereitung in einer Weise zu ermöglichen, die den zwischenmenschlichen Kontakt reduziert und Risikogruppen schützt.

## Features
Das Webfront bietet folgende Features:

* Registrierung als Kunde:in einer Tafel
* Registrierung als Leiter;in einer Tafel
* Als Leiter:in können Ausgabe-Zeitslots erstellt werden an denen Lebensmittel ausgegen werden können.
* Als Kunde:in können Bestellungen für die Zeitslots aufgegeben werden
* Als Leiter:in können Bestellungen für die Zeitslots eingesehen werden

## Architektur des Frontend & Deployment
Code Changes im master branch dieser Repository triggern eine GitLab CI/CD Pipeline mit folgenden Schritten:
* lint
* build
* deploy

Dabei wird die Anwendung mit ```ng build --prod``` gebaut und dann in einen S3 Bucket auf AWS deployt. Dieser kann in Zukunft beliebig durch
einen Webserver ersetzt werden.

## Dependencies
* Bootstrap: Styling
* Query: Dropdowns, Animationen
* ngx-toastr: HTTP Error Messages
* fullcalendar: Slot Einträge verwalten
* Angular Material: Date & Time Picker
* momentjs: Date Formatierung

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `--prod` flag for a production build.
