import { Component, OnInit } from '@angular/core';
import { Order } from 'src/app/shared/types/order.type';
import { OrderService } from 'src/app/shared/services/order.service';

@Component({
  selector: 'app-order-list',
  templateUrl: './order-list.component.html',
  styleUrls: ['./order-list.component.scss']
})
export class OrderListComponent implements OnInit {

  orders: Order[];

  constructor(private orderService: OrderService) { }

  ngOnInit(): void {
    this.getAllOrders();
  }

  getAllOrders() {
    this.orderService.getAllOrders().subscribe(
      response => {
        console.log(response);
        response.forEach(element => {
          console.log(element);
        });
        this.orders = response;
      }
    );
  }

}
