import { Component, OnInit, EventEmitter, Output } from '@angular/core';
import * as moment from 'moment';
import { DateSlot } from 'src/app/shared/types/date-slot.type';
import { DatePicker } from 'src/app/shared/services/date-picker.service';

@Component({
  selector: 'app-date-picker',
  templateUrl: './date-picker.component.html',
  styleUrls: ['./date-picker.component.scss']
})
export class DatePickerComponent implements OnInit {

  @Output() Eslot: EventEmitter<DateSlot> =
    new EventEmitter<DateSlot>();

  datePicked: boolean;
  indexDatePicked: number;
  textwarning: string;
  tafelList: DateSlot[];
  slotSytles: [];

  slotCheckedStyle: string[] = [];
  slotDisplayStyle: string[] = [];
  dotColor: string[] = [];
  dotDisplay: string[] = [];
  availableSlot: boolean[] = [];

  constructor(private dateSlot: DatePicker) { }

  ngOnInit(): void {
    this.dateSlot.getSlots().subscribe(timeslots => {
      this.tafelList = timeslots;
      this.fillStyleArray();
    });

    this.slotCheckedStyle.push('shadow-clicked');
    this.slotCheckedStyle.push('');

    this.dotColor.push('dot-green');
    this.dotColor.push('dot-orange');
    this.dotColor.push('dot-red');



    this.datePicked = false;

  }

  addStyle() {
    this.slotDisplayStyle.push(this.slotCheckedStyle[1]);
  }

  fillStyleArray() {
    for (const slot of this.tafelList) {
      this.slotDisplayStyle.push(this.slotCheckedStyle[1]);
      if (slot.maximumPersons > 4) {
        this.dotDisplay.push(this.dotColor[2]);
        this.availableSlot.push(false);
      } else if (slot.maximumPersons < 3) {
        this.dotDisplay.push(this.dotColor[0]);
        this.availableSlot.push(true);
      } else {
        this.dotDisplay.push(this.dotColor[1]);
        this.availableSlot.push(true);
      }
    }
  }

  selectDate(index: number) {
    if (this.availableSlot[index] === true) {
      this.datePicked = true;
      this.indexDatePicked = index;
      this.resetStyle();
      this.slotDisplayStyle[index] = this.slotCheckedStyle[0];
      this.textwarning = '';
    } else {
      this.textwarning = 'Dieser Slot ist schon voll belegt.';
    }
  }

  sendDate() {
    this.Eslot.emit(this.tafelList[this.indexDatePicked]);
  }

  resetStyle() {
    for (let i = 0; i < this.tafelList.length; i++) {
      this.slotDisplayStyle[i] = this.slotCheckedStyle[1];
    }
  }

}
