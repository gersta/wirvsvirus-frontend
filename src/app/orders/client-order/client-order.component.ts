import { Component, OnInit, AfterViewChecked } from '@angular/core';
import { Router } from '@angular/router';
import { ClientOrder } from '../../shared/services/client-order.service';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { DateSlot } from '../../shared/types/date-slot.type';

@Component({
  selector: 'app-client-order',
  templateUrl: './client-order.component.html',
  styleUrls: ['./client-order.component.scss']
})
export class ClientOrderComponent implements OnInit {
  public form: FormGroup;

  pickedSlot: DateSlot;
  dateSelected: boolean;

  constructor(private clientOrderService: ClientOrder, private router: Router, private formBuilder: FormBuilder) { }

  ngOnInit() {
    this.buildForms();
    this.dateSelected = false;
  }


  submit() {
    this.clientOrderService.sentOrder(1, this.form.controls.date.value, this.pickedSlot, 23).subscribe();
    this.goToPage('orders');
  }

  goToPage(pageName: string) {
    this.router.navigate([`${pageName}`]);
  }

  private buildForms() {
    this.form = this.formBuilder.group({
      comments: ['', []],
      date: ['', []],

    });
  }

  setDate(slot: DateSlot) {
    this.pickedSlot = slot;
    this.dateSelected = true;
  }

}

