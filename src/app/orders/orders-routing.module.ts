import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { OrderListComponent } from './order-list/order-list.component';
import { ClientOrderComponent } from './client-order/client-order.component';


const routes: Routes = [
  {
    path: '',
    component: OrderListComponent,
  },
  {
    path: 'add',
    component: ClientOrderComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class OrdersRoutingModule { }
