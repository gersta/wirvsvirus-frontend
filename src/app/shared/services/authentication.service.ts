import { Injectable } from '@angular/core';
import * as moment from 'moment';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { User } from '../types/user.type';


const ACCESS_TOKEN_POINTER = 'access_token';
const EXPIRES_AT_POINTER = 'expires_at';
const USER_POINTER = 'side_id';

@Injectable({
  providedIn: 'root'
})
export class AuthenticationService {

  constructor(private httpClient: HttpClient) { }

  /**
   * Retrieve the current access token
   */
  public getAccessToken(): string {
    return localStorage.getItem(ACCESS_TOKEN_POINTER);
  }

  /**
   * Retrieve the timestamp when the current access token will expire
   */
  public getExpireDate() {
    const expiresAt = localStorage.getItem(EXPIRES_AT_POINTER);
    return moment.unix(Number(expiresAt));
  }

  /**
   * Set access token for the current user
   * @param accessToken Bearer token
   * @param expiresAt Timestamp when the token expires
   */
  public setAccessToken(accessToken: string, expiresAt: number) {
    accessToken ? localStorage.setItem(ACCESS_TOKEN_POINTER, accessToken) : localStorage.removeItem(ACCESS_TOKEN_POINTER);
    expiresAt ? localStorage.setItem(EXPIRES_AT_POINTER, expiresAt.toString()) : localStorage.removeItem(EXPIRES_AT_POINTER);
  }

  public setUser(user: User) {
    localStorage.setItem(USER_POINTER, JSON.stringify(user));
  }

  public getUser(): User {
    return JSON.parse(localStorage.getItem(USER_POINTER));
  }

  /**
   * Check if the current user token is valid
   */
  public isAuthenticated(): boolean {
    return (this.getAccessToken() && this.getExpireDate() > moment());
  }

  /**
   * Login the current user
   * @param email email of user
   * @param password password of user
   */
  public async login(email, password) {
    const response = await this.httpClient.post(environment.apiEndpoint + 'login', { email, password }).toPromise();
    // tslint:disable-next-line: no-string-literal
    this.setAccessToken(response.toString(), moment().add(30, 'minutes').unix());
    this.setUser(response as User);
    return response;
  }

  /**
   * Logs out the user
   */
  public logout(): void {
    this.setAccessToken(null, null);
  }
}
