import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { Observable, throwError } from 'rxjs';
import { DateSlot } from '../types/date-slot.type';
import { catchError } from 'rxjs/operators';

@Injectable({
    providedIn: 'root'
})
export class DatePicker {
    private slotURL = 'assets/mock/demo.json';

    constructor(private httpClient: HttpClient) { }

    public getSlots(): Observable<DateSlot[]> {
        return this.httpClient.get<DateSlot[]>(this.slotURL).pipe(
            catchError(this.handleError)
        );
    }

    private handleError(err: HttpErrorResponse) {
        let errorMessage = '';
        if (err.error instanceof ErrorEvent) {
            errorMessage = `An error occurred: ${err.error.message}`;
        } else {
            errorMessage = `Server returned code: ${err.status}, error message is: ${err.message}`;
        }
        console.error(errorMessage);
        return throwError(errorMessage);
    }
}
