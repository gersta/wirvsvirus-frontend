import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Site } from '../types/site.type';
import { SiteManager } from '../types/site-manager.type';
import { environment } from 'src/environments/environment';
import { Observable, of } from 'rxjs';

@Injectable({
    providedIn: 'root'
})
export class SiteService {

    constructor(private httpClient: HttpClient) { }

    public createSiteWithManager(site: Site, siteManager: SiteManager): Observable<any> {
        return this.httpClient.post(environment.apiEndpoint + 'sites/', { site, siteManager });
    }

    public getSitesByZipCode(zipCode: string): Observable<Site[]> {
        return this.httpClient.get<Site[]>(environment.apiEndpoint + `sites?zipCode=${zipCode}`);
    }

}
