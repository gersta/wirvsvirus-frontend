import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { Observable } from 'rxjs';
import { Order } from '../types/orderright.type';
import { DateSlot } from '../types/date-slot.type';


@Injectable({
    providedIn: 'root'
})
export class ClientOrder {
    public order: Order = {
        id: 0,
        comments: '',
        slot: null,
        userID: 0
    };

    constructor(private httpClient: HttpClient) { }

    public sentOrder(id: number, commments: string, slot: DateSlot, userId: number): Observable<Order> {
        this.order.id = id;
        this.order.comments = commments;
        this.order.slot = slot;
        this.order.userID = userId;
        return this.httpClient.post<Order>(environment.apiEndpoint + '/orders/', this.order);
      }
}
