import { Injectable } from '@angular/core';
import { Customer } from '../types/customer.type';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { SiteManager } from '../types/site-manager.type';

@Injectable({
    providedIn: 'root'
})
export class UserService {

    constructor(private httpClient: HttpClient) { }

    public createCustomer(customer: Customer): Observable<any> {
        return this.httpClient.post(environment.apiEndpoint + 'users/customers', customer);
    }

    public createSiteManager(siteManager: SiteManager): Observable<any> {
        return this.httpClient.post(environment.apiEndpoint + 'users/siteManagers', siteManager);
    }

}
