import { Injectable } from '@angular/core';
import { DateSlot } from '../types/date-slot.type';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class SlotService {

  constructor(private httpClient: HttpClient) {}

  public createSlot(slot: DateSlot): Observable<any> {
    return this.httpClient.post(`${environment.apiEndpoint}/sites/${slot.siteId}/timeslots`, {slot});
  }
}
