import { Pipe, PipeTransform } from '@angular/core';
import * as moment from 'moment';

@Pipe({
    name: 'pipedateString'
})
export class DateStringPipe implements PipeTransform {
    transform(value: moment.Moment): string {
        moment.locale('de-DE');
        return value.format('dddd');
    }
}
