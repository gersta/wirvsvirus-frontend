import { DateSlot } from './date-slot.type';
import { Customer } from './customer.type';

export interface Order {
  slotId: number;
  comment: string;
  customerId: number;
  collector: string;
  customer: Customer;
  timeslot: DateSlot;
}
