export interface Site {
    id: number;
    address: string;
    zipCode: string;
    homepage: string;
    maximumPortions: number;
    name: string;
    repetitions: number;
}
