import { DateSlot } from './date-slot.type';

export interface Order {
  // tafelId: number;
  // noAdults: number;
  // noKids: number;
  // // Lebensmittel
  // groceries: string;
  // // Unverträglichkeiten und Allergien
  // allergies: string;
  // // Date and Time of pickup
  // pickUpTime: Date;
  // // date the order was placed
  // orderTime: Date;
  id: number;
  comments: string;
  slot: DateSlot;
  userID: number;

}
