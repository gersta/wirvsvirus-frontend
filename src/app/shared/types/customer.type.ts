import { User } from './user.type';

export interface Customer extends User {
    isSiteManager: boolean;
    numberAdults: number;
    numberChilds: number;
    zipCode: string;
    preferences: string[];
}
