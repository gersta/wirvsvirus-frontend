export interface User {
    id: number;
    email: string;
    emailConfirmed: boolean;
    firstName: string;
    lastName: string;
    password?: string;
    isSiteManager: boolean;
    isVerified: boolean;
    siteId?: number;
}
