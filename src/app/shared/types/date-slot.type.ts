import { Moment } from 'moment';

export interface DateSlot {
    siteId: number;
    maximumPersons: number;
    currentPersons: number;
    startTime: Moment;
    endTime: Moment;
}
