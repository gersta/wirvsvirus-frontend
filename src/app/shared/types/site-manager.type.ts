import { User } from './user.type';

export interface SiteManager extends User {
    phoneNumber: string;
}
