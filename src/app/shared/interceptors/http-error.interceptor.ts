import { Injectable } from '@angular/core';
import {
    HttpEvent, HttpRequest, HttpHandler,
    HttpInterceptor, HttpErrorResponse
} from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { retry, catchError } from 'rxjs/operators';
import { ToastrService } from 'ngx-toastr';

@Injectable()
export class HttpErrorInterceptor implements HttpInterceptor {

    constructor(private toastrService: ToastrService) { }

    intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {

        return next.handle(request).pipe(
            catchError((error: HttpErrorResponse) => {
                if (error.error && error.error.status !== 403) {
                    if (error.error instanceof ErrorEvent) {
                        // client-side/network error
                        this.toastrService.warning('Netzwerkprobleme');
                    } else {
                        // unsuccessful respond code from server
                        this.toastrService.error('Interner Server Fehler');
                    }
                }
                return throwError(error);
            })
        );
    }
}
