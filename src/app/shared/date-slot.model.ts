import { Moment } from 'moment';

export interface DateSlot {
    begin: Moment;
    end: Moment;
}
