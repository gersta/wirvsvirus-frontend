import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HttpClientModule } from '@angular/common/http';
import { DateStringPipe } from './pipes/date.pipe';



@NgModule({
  declarations: [DateStringPipe],
  imports: [
    CommonModule,
    HttpClientModule
  ],
  exports: [DateStringPipe]
})
export class SharedModule { }
