import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import * as moment from 'moment';
import { SlotService } from '../../shared/services/slot.service';
import { DateSlot } from 'src/app/shared/types/date-slot.type';

@Component({
  selector: 'app-create-slot',
  templateUrl: './create-slot.component.html',
  styleUrls: ['./create-slot.component.scss']
})
export class CreateSlotComponent implements OnInit {

  public createSlotForm: FormGroup;
  public submitted = false;
  public error: string;

  constructor(private formBuilder: FormBuilder, private slotService: SlotService) { }

  ngOnInit(): void {
    this.initForm();
  }

  private initForm() {
    this.createSlotForm = this.formBuilder.group({
      day: ['', [Validators.required]],
      from: ['', [Validators.required]],
      to: ['', [Validators.required]],
      maxNum: ['', [Validators.required]]
    });
  }

  submit() {
    const day: string = this.createSlotForm.get('day').value;
    const from: string = this.createSlotForm.get('from').value;
    const to: string = this.createSlotForm.get('to').value;
    const maxNum: number = this.createSlotForm.get('maxNum').value;

    // date and time separated by whitespace here
    const momentFrom = moment(`${day} ${from}`, 'YYYY-MM-DD HH:mm');
    const momentTo = moment(`${day} ${to}`, 'YYYY-MM-DD HH:mm');

    const slot: DateSlot = {
      startTime: momentFrom,
      endTime: momentTo,
      currentPersons: 0,
      siteId: 0,
      maximumPersons: maxNum
    };

    this.slotService.createSlot(slot);
  }

  myFilter = (d: Date | null): boolean => {
    const day = (d || new Date()).getDay();
    // Prevent Saturday and Sunday from being selected.
    return day !== 0 && day !== 6;
  }

}
