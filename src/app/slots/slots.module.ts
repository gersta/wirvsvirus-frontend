import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SlotsRoutingModule } from './slots-routing.module';
import { CalendarComponent } from './calendar/calendar.component';
import { FullCalendarModule } from '@fullcalendar/angular';
import { CreateSlotComponent } from './create-slot/create-slot.component';
import { NgxMaterialTimepickerModule } from 'ngx-material-timepicker';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NoopAnimationsModule } from '@angular/platform-browser/animations';
import { MatDatepickerModule } from '@angular/material/datepicker';
import { MatInputModule } from '@angular/material/input';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatNativeDateModule } from '@angular/material/core';


@NgModule({
  declarations: [CalendarComponent, CreateSlotComponent],
  imports: [
    CommonModule,
    FullCalendarModule,
    SlotsRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    NgxMaterialTimepickerModule.setLocale('de-DE'),
    MatDatepickerModule,
    MatFormFieldModule,
    MatNativeDateModule,
    MatInputModule,
  ]
})
export class SlotsModule { }
