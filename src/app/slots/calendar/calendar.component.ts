import { Component, OnInit, ViewChild, AfterViewInit } from '@angular/core';
import dayGridPlugin from '@fullcalendar/daygrid';
import timeGridPlugin from '@fullcalendar/timegrid';
import { FullCalendarComponent } from '@fullcalendar/angular';
import { EventInput } from '@fullcalendar/core';

@Component({
  selector: 'app-calendar',
  templateUrl: './calendar.component.html',
  styleUrls: ['./calendar.component.scss']
})
export class CalendarComponent implements OnInit, AfterViewInit {


  @ViewChild('calendar') calendarComponent: FullCalendarComponent;

  calendarPlugins = [dayGridPlugin, timeGridPlugin];

  calendarEvents: EventInput[] = [
    { title: 'Slot 1', start: new Date() }
  ];

  constructor() { }

  ngOnInit(): void {

  }

  ngAfterViewInit(): void {
    const calendarApi = this.calendarComponent.getApi();
    calendarApi.setOption('locale', 'de-de');

    // this.calendarEvents.push(
    //   { title: 'Event Now', start: new Date() }
    // );
  }

}
