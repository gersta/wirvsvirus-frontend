import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AnonymousGuard } from './shared/guards/anonymous.guard';
import { AuthGuard } from './shared/guards/auth.guard';
import { CalendarComponent } from './slots/calendar/calendar.component';
import { CreateSlotComponent } from './slots/create-slot/create-slot.component';

const routes: Routes = [
  {
    path: '',
    pathMatch: 'full',
    redirectTo: 'orders'
  },
  {
    path: 'auth',
    loadChildren: () => import('./auth/auth.module')
      .then(m => m.AuthModule),
    canActivate: [AnonymousGuard]
  },
  {
    path: 'orders',
    loadChildren: () => import('./orders/orders.module')
      .then(m => m.OrdersModule),
    canActivate: [AuthGuard]
  },
  {
    path: 'calendar',
    component: CalendarComponent
  },
  {
    path: 'slots',
    loadChildren: () => import('./slots/slots.module')
      .then(m => m.SlotsModule),
    canActivate: [AuthGuard]
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
