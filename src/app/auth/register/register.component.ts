import { Component, OnInit, OnDestroy } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { MustMatch } from 'src/app/shared/validators/must-match.validator';
import { UserService } from 'src/app/shared/services/user.service';
import { Customer } from 'src/app/shared/types/customer.type';
import { SiteManager } from 'src/app/shared/types/site-manager.type';
import { User } from 'src/app/shared/types/user.type';
import { SiteService } from 'src/app/shared/services/site.service';
import { Site } from 'src/app/shared/types/site.type';
import { AuthenticationService } from 'src/app/shared/services/authentication.service';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { Observable, Subject } from 'rxjs';
import { takeUntil, distinctUntilChanged, debounceTime, filter, switchMap } from 'rxjs/operators';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss']
})
export class RegisterComponent implements OnInit, OnDestroy {

  constructor(
    private formBuilder: FormBuilder,
    private userService: UserService,
    private siteService: SiteService,
    private authService: AuthenticationService,
    private router: Router,
    private notificationService: ToastrService
  ) { }

  public registerForm: FormGroup;
  public submitted = false;
  public isManager = false;
  public error: string;

  keyUp$ = new Subject<string>();
  destroy$ = new Subject<boolean>();

  sitesToChoose: Site[];
  siteIdChosen: number;
  showOptions = false;

  ngOnInit(): void {
    this.createForm();
    this.registerTypeAhead();
  }

  ngOnDestroy() {
    // unsub all subs
    this.destroy$.next(true);
    this.destroy$.unsubscribe();
  }

  private createForm() {
    this.registerForm = this.formBuilder.group({
      // General
      firstName: ['', [Validators.required, Validators.maxLength(255)]],
      lastName: ['', [Validators.required, Validators.maxLength(255)]],
      email: ['', [Validators.required, Validators.minLength(4), Validators.maxLength(255), Validators.email]],
      password: ['', [Validators.required, Validators.minLength(6), Validators.maxLength(255)]],
      passwordConfirmation: [''],

      // Group 1
      zipCode: ['', [Validators.required, Validators.minLength(5), Validators.minLength(5)]],
      numberOfPersons: [1, [Validators.required]],
      incompatibilities: [''],

      // Group 2 conditional validation set
      fbName: [''],
      fbStreet: [''],
      fbStreetNumber: [''],
      fbZipCode: [''],
      fbLocation: [''],
      fbContact: [''],
      fbPhone: [''],
      fbWebsite: [''],
    }, { validator: MustMatch('password', 'passwordConfirmation') });
  }

  /**
   * the input fields sends its value to the keyUp$ subject which is then piped here
   * if more than 2 chars are written and they are distinct from the last value, a backend request is sent
   * to fatch all matching sites which are then displayed in an option list to the user
   */
  registerTypeAhead() {
    this.keyUp$.pipe(
      takeUntil(this.destroy$),
      debounceTime(200),
      distinctUntilChanged(),
      filter((zipCode: string) => zipCode.length > 1),
      switchMap(zipCode => this.siteService.getSitesByZipCode(zipCode))
    ).subscribe((sites: Site[]) => {
      this.sitesToChoose = sites;
      this.showOptions = true;
    });
  }

  chooseSite(id: number, zipCode: number) {
    this.siteIdChosen = id;
    this.registerForm.controls.zipCode.setValue(zipCode);
    this.showOptions = false;
  }

  public submit() {
    this.submitted = true;

    const user: User = {
      id: 0,
      email: this.registerForm.controls.email.value,
      emailConfirmed: true,
      firstName: this.registerForm.controls.firstName.value,
      lastName: this.registerForm.controls.lastName.value,
      password: this.registerForm.controls.password.value,
      isSiteManager: this.isManager,
      isVerified: true,
    };

    if (this.isManager) {

      const siteManager: SiteManager = {
        ...user,
        phoneNumber: this.registerForm.controls.fbPhone.value,
      };

      const site: Site = {
        address: this.registerForm.controls.fbStreet.value + ' ' +
          this.registerForm.controls.fbStreetNumber.value + ', ' +
          this.registerForm.controls.fbZipCode.value + ' ' +
          this.registerForm.controls.fbLocation.value,
        zipCode: this.registerForm.controls.fbZipCode.value,
        homepage: this.registerForm.controls.fbWebsite.value,
        maximumPortions: 0,
        name: this.registerForm.controls.fbName.value,
        id: this.siteIdChosen,
        repetitions: 0
      };

      this.siteService.createSiteWithManager(site, siteManager).subscribe(result => {
        console.log(result);
        if (result.siteManager !== -1) {
          this.authService.login(user.email, user.password).then(() => this.router.navigate(['/']));
          this.notificationService.success('Der Nutzer wurde erfolgreich angelegt.');
        } else {
          this.error = 'Die E-Mail exestiert bereits';
          this.submitted = false;
        }
      }, error => console.log(error));

    } else {

      const customer: Customer = {
        ...user,
        numberAdults: this.registerForm.controls.numberOfPersons.value,
        numberChilds: 0,
        zipCode: this.registerForm.controls.zipCode.value,
        isSiteManager: false,
        preferences: [this.registerForm.controls.incompatibilities.value],
        siteId: this.siteIdChosen
      };

      this.userService.createCustomer(customer).subscribe(result => {
        this.authService.login(user.email, user.password).then(() => this.router.navigate(['/']));
        this.notificationService.success('Der Nutzer wurde erfolgreich angelegt.');
      }, error => {
        this.error = error;
        this.submitted = false;
      });

    }
  }

  public onlyAllowNumbers(event) {
    const key = event.charCode || event.keyCode || 0;
    return (
      key === 8 ||
      key === 9 ||
      key === 13 ||
      key === 46 ||
      key === 110 ||
      key === 190 ||
      (key >= 35 && key <= 40) ||
      (key >= 48 && key <= 57) ||
      (key >= 96 && key <= 105));
  }

  public setIsManager(value: boolean) {
    this.isManager = value;

    if (value) {
      this.registerForm.controls.zipCode.setValidators(null);
      this.registerForm.controls.numberOfPersons.setValidators(null);
      this.registerForm.controls.fbName.setValidators([Validators.required, Validators.maxLength(255)]);
      this.registerForm.controls.fbStreet.setValidators([Validators.required, Validators.maxLength(255)]);
      this.registerForm.controls.fbStreetNumber.setValidators([Validators.required, Validators.maxLength(10)]);
      this.registerForm.controls.fbZipCode.setValidators([Validators.required, Validators.minLength(5), Validators.minLength(5)]);
      this.registerForm.controls.fbLocation.setValidators([Validators.required, Validators.maxLength(255)]);
      this.registerForm.controls.fbContact.setValidators([Validators.required, Validators.maxLength(1000)]);
      this.registerForm.controls.fbPhone.setValidators([
        Validators.required,
        Validators.pattern('^[+]*[(]{0,1}[0-9]{1,4}[)]{0,1}[-\s\./0-9]*$')]);
      this.registerForm.controls.fbContact.setValidators([Validators.maxLength(255)]);
    } else {
      this.registerForm.controls.zipCode.setValidators([Validators.required, Validators.minLength(5), Validators.minLength(5)]);
      this.registerForm.controls.numberOfPersons.setValidators([Validators.required]);
      this.registerForm.controls.fbName.setValidators(null);
      this.registerForm.controls.fbStreet.setValidators(null);
      this.registerForm.controls.fbStreetNumber.setValidators(null);
      this.registerForm.controls.fbZipCode.setValidators(null);
      this.registerForm.controls.fbLocation.setValidators(null);
      this.registerForm.controls.fbContact.setValidators(null);
      this.registerForm.controls.fbPhone.setValidators(null);
    }

    this.registerForm.controls.zipCode.updateValueAndValidity();
    this.registerForm.controls.numberOfPersons.updateValueAndValidity();
    this.registerForm.controls.fbName.updateValueAndValidity();
    this.registerForm.controls.fbStreet.updateValueAndValidity();
    this.registerForm.controls.fbStreetNumber.updateValueAndValidity();
    this.registerForm.controls.fbZipCode.updateValueAndValidity();
    this.registerForm.controls.fbLocation.updateValueAndValidity();
    this.registerForm.controls.fbContact.updateValueAndValidity();
    this.registerForm.controls.fbPhone.updateValueAndValidity();
    this.registerForm.controls.fbWebsite.updateValueAndValidity();
  }

}
