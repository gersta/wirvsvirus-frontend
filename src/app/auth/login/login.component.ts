import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { AuthenticationService } from 'src/app/shared/services/authentication.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  public loginForm: FormGroup;
  public submitted = false;
  public error: string;

  constructor(
    private formBuilder: FormBuilder,
    private authService: AuthenticationService,
    private router: Router,
  ) { }

  ngOnInit(): void {
    this.createForm();
  }

  private createForm() {
    this.loginForm = this.formBuilder.group({
      email: ['', Validators.required],
      password: ['', Validators.required]
    });
  }

  public submit() {
    this.submitted = true;

    this.authService.login(this.loginForm.controls.email.value, this.loginForm.controls.password.value)
      .then(result => {
        this.router.navigate(['']);
      }).catch(error => {
        if (error.status === 403) {
          this.error = 'Die angegebenen Zugangssdaten sind nicht korrekt';
        }
      });

    this.submitted = false;
  }

}
