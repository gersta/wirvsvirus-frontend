import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { Router, NavigationEnd } from '@angular/router';
import { AuthenticationService } from 'src/app/shared/services/authentication.service';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.scss']
})
export class NavbarComponent implements OnInit {

  @ViewChild('nav-link', { static: true })
  public navLink: ElementRef;

  public username: string;
  public isLoggedIn: boolean;

  constructor(
    private authService: AuthenticationService,
    private router: Router,
  ) { }

  ngOnInit() {
    this.updateUserNavbar();
  }

  private updateUserNavbar() {
    this.router.events.subscribe((event) => {
      if (event instanceof NavigationEnd) {
        if (this.authService.isAuthenticated()) {
          this.isLoggedIn = true;
        }
      }
    });
  }

  public logout() {
    this.authService.logout();
    this.isLoggedIn = false;
    this.router.navigate(['auth/login']);
  }

  public close() {
    ($('.navbar-collapse') as any).collapse('hide');
  }


}
